/*
  Input:
    3
    (a+(b*c))
    ((a+b)*(z+x))
    ((a+t)*((b+(a+c))^(c+d)))

  Output:
    abc*+
    ab+zx+*
    at+bac++cd+^*
*/

#include <iostream>
using namespace std;

class Stack {
  char chars[500];
  char *nxt;
public:
  Stack();
  void push(char chr);
  char pop();
};

Stack::Stack() {
  nxt = chars;
}

void Stack::push(char chr) {
  *nxt = chr;
  nxt++;
}

char Stack::pop() {
  nxt--;
  return *nxt;
}

int main() {

  int num_input;
  cin >> num_input;

  Stack stack;

  string input;
  for (int i = 0; i < num_input; i++) {
    cin >> input;

    int in_len = input.length();
    for (register int j = 0; j < in_len; j++) {

      register char chr = input[j];

      if (chr == '(') continue;
      if (chr == ')') {
        cout << stack.pop();
      }
      else if (chr == '+'
          || chr == '-'
          || chr == '*'
          || chr == '/'
          || chr == '^') {
        stack.push(chr);
      }
      else {
        cout << chr;
      }
    }
    cout << endl;
  }
  return 0;
}
