#SPOJ

All my answers to [SPOJ](spoj.com) problems.

| problem                                 | date     | time | mem  |
|:---------------------------------------:|:--------:|:----:|:----:|
| [Life, the Universe, and Everything][1] | 21/05/14 | 0.00 | 2.6M |
| [Transform the Expression][2]           | 24/05/14 | 0.02 | 2.8M |

[1]: http://www.spoj.com/problems/TEST/
[2]: http://www.spoj.com/problems/ONP/
